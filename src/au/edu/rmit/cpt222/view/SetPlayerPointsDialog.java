package au.edu.rmit.cpt222.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import au.edu.rmit.cpt222.controller.SetPlayerPointsDialogController;

/**
* @author Paolo DiPietro
* 
* SetPlayerPointsDialog class is a control set for 
* for the GUI that sets player pounts during runtime
*/
@SuppressWarnings("serial")
public class SetPlayerPointsDialog extends JDialog 
{
	private MainView mainView;
	private SetPlayerPointsDialogController setPlayerPointsDialogController;
	private JTextField playerPointsTextField = new JTextField();

	public SetPlayerPointsDialog(MainView mainView) 
	{
		this.mainView = mainView;
		setPlayerPointsDialogController = new SetPlayerPointsDialogController(this, mainView);
		setTitle("Set Player Points");
		setSize(350, 120);
		// prevent the user from resizing the frame
		setResizable(false);	
		// APPLICATION_MODAL blocks all input to the top level window
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		// set the layout of the dialog box
		GridLayout gridLayout = new GridLayout(); 
		gridLayout.setColumns(2);
		gridLayout.setRows(2);
		
		// create button
		JButton setPlayerPointsButton = new JButton("Set Player Points");		
		// create label
		JLabel playerPointsLabel = new JLabel("Player Points");		
		// create panels to contain label
		JPanel playerPointsLabelPanel = new JPanel();				
		// create panel to contain text field
		JPanel playerPointsTextFieldPanel = new JPanel();	
		
		// set layout and add the panels
		playerPointsLabelPanel.setLayout(new BorderLayout());		
		playerPointsLabelPanel.add(playerPointsLabel, BorderLayout.NORTH);		
		playerPointsTextFieldPanel.setLayout(new BorderLayout());						
		playerPointsTextFieldPanel.add(playerPointsTextField, BorderLayout.NORTH);
		
		// create panel to contain all panels
		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(10,10,10,10));
		mainPanel.setLayout(gridLayout);
		
		// add all panels to the main panel
		mainPanel.add(playerPointsLabelPanel);
		mainPanel.add(playerPointsTextFieldPanel);
		mainPanel.add(setPlayerPointsButton);
		
		add(mainPanel);
		setLocationRelativeTo(this);
		
		// register the JButton with action listeners.
		setPlayerPointsButton.addActionListener(setPlayerPointsDialogController);
		
		// registerKeyboardAction allows user to interact with JButton using enter/spacebar key
		setPlayerPointsButton.registerKeyboardAction(setPlayerPointsButton.getActionForKeyStroke(
        KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
        KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
        JComponent.WHEN_FOCUSED);
		setPlayerPointsButton.registerKeyboardAction(setPlayerPointsButton.getActionForKeyStroke(
        KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
        KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
        JComponent.WHEN_FOCUSED);
	}
	
	public MainView getMainView() 
	{
		return mainView;
	}
	
	public String getCreditPoints()
	{
		return playerPointsTextField.getText();
	}
}
