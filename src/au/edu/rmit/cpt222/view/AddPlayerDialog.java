package au.edu.rmit.cpt222.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;

import au.edu.rmit.cpt222.controller.AddPlayerDialogController;

/**
 * @author Paolo DiPietro
 * 
 * AddPlayerDialog class is a popup dialog with a 
 * control set for adding a player to the game
 */
@SuppressWarnings("serial")
public class AddPlayerDialog extends JDialog
{
	private MainView mainView;
	private AddPlayerDialogController addPlayerDialogController;
	private JTextField playerIDTextField = new JTextField();
    private JTextField playerNameTextField = new JTextField();
	private JTextField creditPointsTextField = new JTextField();
	
	public AddPlayerDialog(MainView mainView)
	{
		this.mainView = mainView;
		addPlayerDialogController = new AddPlayerDialogController(this, mainView);
		setTitle("Add Player to Game");
		setSize(350, 150);
		// prevent the user from resizing the frame
		setResizable(false);	
		// APPLICATION_MODAL blocks all input to the top level window
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		// set the layout of the dialog box
		GridLayout gridLayout = new GridLayout(); 
		gridLayout.setColumns(2);
		gridLayout.setRows(4);
		
		// create button
		JButton addPlayerButton = new JButton("Add Player");
		
		// create labels
		JLabel playerIDLabel = new JLabel("Player ID");
		JLabel playerNameLabel = new JLabel("Player Name");
		JLabel creditPointsLabel = new JLabel("Credit Points");	
		
		// create panels to contain labels
		JPanel playerIDLabelPanel = new JPanel();
		JPanel playerNameLabelPanel = new JPanel();
		JPanel creditPointsLabelPanel = new JPanel();
				
		// create panels to contain text fields
		JPanel playerIDTextFieldPanel = new JPanel();
		JPanel playerNameTextFieldPanel = new JPanel();
		JPanel creditPointsTextFieldPanel = new JPanel();		
		
		// set layout and add the panels
		playerIDLabelPanel.setLayout(new BorderLayout());
		playerNameLabelPanel.setLayout(new BorderLayout());
		creditPointsLabelPanel.setLayout(new BorderLayout());
		
		playerIDLabelPanel.add(playerIDLabel, BorderLayout.NORTH);
		playerNameLabelPanel.add(playerNameLabel, BorderLayout.NORTH);
		creditPointsLabelPanel.add(creditPointsLabel, BorderLayout.NORTH);
		
		playerIDTextFieldPanel.setLayout(new BorderLayout());
		playerNameTextFieldPanel.setLayout(new BorderLayout());
		creditPointsTextFieldPanel.setLayout(new BorderLayout());
						
		playerIDTextFieldPanel.add(playerIDTextField, BorderLayout.NORTH);
		playerNameTextFieldPanel.add(playerNameTextField, BorderLayout.NORTH);
		creditPointsTextFieldPanel.add(creditPointsTextField, BorderLayout.NORTH);
		
		// create panel to contain all panels
		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(10,10,10,10));
		mainPanel.setLayout(gridLayout);
		
		// add all panels to the main panel
		mainPanel.add(playerIDLabelPanel);
		mainPanel.add(playerIDTextFieldPanel);
		mainPanel.add(playerNameLabelPanel);
		mainPanel.add(playerNameTextFieldPanel);
		mainPanel.add(creditPointsLabelPanel);
		mainPanel.add(creditPointsTextFieldPanel);
		mainPanel.add(addPlayerButton);
		
		add(mainPanel);
		setLocationRelativeTo(this);
		
		// register the JButton with action listeners.
		addPlayerButton.addActionListener(addPlayerDialogController);
		
		// registerKeyboardAction allows user to interact with JButton using enter/spacebar key
		addPlayerButton.registerKeyboardAction(addPlayerButton.getActionForKeyStroke(
        KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
        KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
        JComponent.WHEN_FOCUSED);
		addPlayerButton.registerKeyboardAction(addPlayerButton.getActionForKeyStroke(
        KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
        KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
        JComponent.WHEN_FOCUSED);
	}
	
	public MainView getMainView() 
	{
		return mainView;
	}
	
	public String getPlayerID()
	{
		return playerIDTextField.getText();
	}
	
	public String getPlayerName()
	{
		return playerNameTextField.getText();
	}
	
	public String getCreditPoints()
	{
		return creditPointsTextField.getText();
	}
}
