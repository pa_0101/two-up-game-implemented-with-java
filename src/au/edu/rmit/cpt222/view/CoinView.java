package au.edu.rmit.cpt222.view;

import java.awt.BorderLayout;

import javax.swing.JFrame;

/*
* CoinDisplayArea is responsible for displaying the coin 
* images
*/
@SuppressWarnings("serial")
public class CoinView extends JFrame 
{
	private static CoinDisplayArea displayArea;
	
	public CoinView()
	{
		setTitle("Coin View");
		setSize(400, 200);
		
		setLocation(600, 110);
		// prevent the user from resizing the frame
		setResizable(false);
		setLayout(new BorderLayout());
		
		displayArea = new CoinDisplayArea(this);
		addDisplayArea();		
		
		setVisible(true);
	}
	
	public static CoinDisplayArea getDisplayArea()
	{
		return displayArea;
	}	
	
	public void removeDisplayArea()
	{
		displayArea.repaint();
	}
	
	public void addDisplayArea()
	{
		add(displayArea, BorderLayout.CENTER);
	}
}
