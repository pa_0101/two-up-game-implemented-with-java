package au.edu.rmit.cpt222.view;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
* @author Paolo DiPietro
* 
* GameEngineServerView class is a control set for 
* for the GUI that displays the servers connection details
*/
@SuppressWarnings("serial")
public class GameEngineServerView extends JFrame 
{
	private JTextArea jTextArea;
	
	public GameEngineServerView()
	{	
		jTextArea = new JTextArea();
		
		setTitle("Multi Player Server");
		setSize(420, 100);
		setResizable(false);
		
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(new JScrollPane(jTextArea), BorderLayout.CENTER);
		setVisible(true);
		
		// prompt the user to confirm closing the server
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)   
			{
				JFrame frame = (JFrame)e.getSource();

				int result = JOptionPane.showConfirmDialog(frame,
						"Are you sure you want to close the server?",
						"Close Server",
						JOptionPane.YES_NO_OPTION);

				if(result == JOptionPane.YES_OPTION)
				{
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				}
			}
		});	
	}
	
	public void printToServerGUI(String serverInfo)
	{
		jTextArea.append(serverInfo);
	}
}
