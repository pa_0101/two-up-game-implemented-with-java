package au.edu.rmit.cpt222.view;

import java.awt.Color;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import au.edu.rmit.cpt222.controller.ToolBarController;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;

/**
 * @author Paolo DiPietro
 * 
 * ToolBar class is a control set for the main functions
 * in the game 
 */
@SuppressWarnings("serial")
public class ToolBar extends JPanel 
{
	private MainView mainView;
	private GameEngine gameEngine;
	private ToolBarController controller;
	private JButton addPlayer;
	private JButton placeBet;
	private JButton flipCoins;
	private JButton resetGame;
	private JButton setPlayerPoints;
	private JButton displayHistory;
	
	public ToolBar(MainView mainView, GameEngine gameEngine)
	{
		this.mainView = mainView;
		controller = new ToolBarController(this);
		setBackground(Color.DARK_GRAY);
		
		addPlayer = new JButton("Add Player");
		add(addPlayer);
		placeBet = new JButton("Place Bet");
		add(placeBet);
		flipCoins = new JButton("Flip Coins");
		add(flipCoins);
		resetGame = new JButton("Reset Game");
		add(resetGame);
		setPlayerPoints = new JButton("Set Player Points");
		add(setPlayerPoints);
		displayHistory = new JButton("Display History");
		add(displayHistory);		
		
		// registerKeyboardAction allows user to interact with JButton using enter/spacebar key
		addPlayer.registerKeyboardAction(addPlayer.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		addPlayer.registerKeyboardAction(addPlayer.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
		placeBet.registerKeyboardAction(placeBet.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		placeBet.registerKeyboardAction(placeBet.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
		flipCoins.registerKeyboardAction(placeBet.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		flipCoins.registerKeyboardAction(placeBet.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
		resetGame.registerKeyboardAction(placeBet.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		resetGame.registerKeyboardAction(placeBet.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
		setPlayerPoints.registerKeyboardAction(placeBet.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		setPlayerPoints.registerKeyboardAction(placeBet.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
		displayHistory.registerKeyboardAction(displayHistory.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		displayHistory.registerKeyboardAction(displayHistory.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);	
		
		// register the buttons with listeners
		addPlayer.addActionListener(controller);
		placeBet.addActionListener(controller);
		flipCoins.addActionListener(controller);
		resetGame.addActionListener(controller);
		setPlayerPoints.addActionListener(controller);
		displayHistory.addActionListener(controller);
	}
	
	public MainView getMainView() 
	{
		return mainView;
	}
	
	public GameEngine getGameEngine() 
	{
		return gameEngine;
	}
}
