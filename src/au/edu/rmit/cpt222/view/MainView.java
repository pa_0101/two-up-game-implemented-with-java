package au.edu.rmit.cpt222.view;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import au.edu.rmit.cpt222.model.comms.GameEngineClientStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;

/*
 * MainView is responsible for instantiating and laying 
 * out all the main GUI components.
 */
@SuppressWarnings("serial")
public class MainView extends JFrame 
{
	private static GameEngine gameEngineClientStub;
	private PullDownMenu menu;
	private ToolBar toolBar; 
	private static StatusBar statusBar;
	
	public MainView()
	{		
		gameEngineClientStub = new GameEngineClientStub();
		
		setTitle("CPT222 | Two Up Game");
		setSize(800, 120);
		// center the frame
		setLocationRelativeTo(null); 	
		// prevent the user from resizing the frame
		setResizable(false);
		setLayout(new BorderLayout());		
		
		// prompt the user to confirm exiting the game
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)   
			{
				JFrame frame = (JFrame)e.getSource();

				int result = JOptionPane.showConfirmDialog(frame,
						"Are you sure you want to exit the game?",
						"Exit Game",
						JOptionPane.YES_NO_OPTION);

				if(result == JOptionPane.YES_OPTION)
				{
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					Player currentPlayer = gameEngineClientStub.getCurrentPlayer();
					gameEngineClientStub.removePlayer(currentPlayer);	
				}
			}
		});			
		
		// set the menu bar
		menu = new PullDownMenu(this);
		setJMenuBar(menu);
	
		toolBar = new ToolBar(this, gameEngineClientStub);
		add(toolBar, BorderLayout.NORTH);	
		
		statusBar = new StatusBar(this);
		add(statusBar, BorderLayout.SOUTH);
	}
	
	public static GameEngine getGameEngine() 
	{
		return gameEngineClientStub;
	}
		
	public MainView getMainView()
	{
		return this;
	}
	
	public static void refreshStatusBar(String gameInfo)
	{		
		if(statusBar != null)
		{
			statusBar.update(gameInfo);
		}
	}
}
