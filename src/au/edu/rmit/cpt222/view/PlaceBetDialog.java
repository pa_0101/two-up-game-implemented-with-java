package au.edu.rmit.cpt222.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;

import au.edu.rmit.cpt222.controller.PlaceBetDialogController;

/**
 * @author Paolo DiPietro
 * 
 * PlaceBetDialog class is a pop up dialog panel  with a 
 * control set for placing a bet in the game
 */
@SuppressWarnings("serial")
public class PlaceBetDialog extends JDialog 
{
	private MainView mainView;
	private PlaceBetDialogController placeBetDialogController;
	private JTextField betAmountTextField = new JTextField();
	private JRadioButton radioHeads = new JRadioButton();
	private JRadioButton radioTails = new JRadioButton();
	private ButtonGroup buttonGroup;
	
	public PlaceBetDialog(MainView mainView)
	{
		this.mainView = mainView;
		placeBetDialogController = new PlaceBetDialogController(this, mainView);
		setTitle("Place Bet");
		setSize(350, 150);		
		// prevent the user from resizing the dialog box
		setResizable(false);		
		// APPLICATION_MODAL blocks all input to the top level window
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		// set the layout of the dialog box
		GridLayout gridLayout = new GridLayout(); 
		gridLayout.setColumns(2);
		gridLayout.setRows(3);
		
		// create button
		JButton placeBetButton = new JButton("Place Bet");
		
		// create labels
		JLabel betAmountLabel = new JLabel("Bet Amount");
		JLabel coinFaceTypeLabel = new JLabel("Coin Face");
		
		// create panels to contain labels
		JPanel betAmountLabelPanel = new JPanel();
		JPanel coinFaceTypeLabelPanel = new JPanel();
		
		// create panels to data entry fields
		JPanel betAmountTextFieldPanel = new JPanel();
		JPanel coinFaceRadioButtonPanel = new JPanel();
		
		//instantiate course radio buttons 
		radioHeads = new JRadioButton("Heads");
		radioTails = new JRadioButton("Tails");
		radioHeads.setSelected(true);
		buttonGroup = new ButtonGroup();		
        buttonGroup.add(radioHeads);		
        buttonGroup.add(radioTails);
		
		// set layout and add the panels
		betAmountLabelPanel.setLayout(new BorderLayout());
		coinFaceTypeLabelPanel.setLayout(new BorderLayout());
		
		betAmountLabelPanel.add(betAmountLabel, BorderLayout.NORTH);
		coinFaceTypeLabelPanel.add(coinFaceTypeLabel, BorderLayout.NORTH);
		
		betAmountTextFieldPanel.setLayout(new BorderLayout());
		coinFaceRadioButtonPanel.setLayout(new BorderLayout());
						
		betAmountTextFieldPanel.add(betAmountTextField, BorderLayout.NORTH);
		coinFaceRadioButtonPanel.add(radioHeads, BorderLayout.WEST);
		coinFaceRadioButtonPanel.add(radioTails, BorderLayout.EAST);
		
		// create panel to contain all panels
		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(10,10,10,10));
		mainPanel.setLayout(gridLayout);
		
		// add all panels to the main panel
		mainPanel.add(betAmountLabelPanel);
		mainPanel.add(betAmountTextFieldPanel);
		mainPanel.add(coinFaceTypeLabelPanel);
		mainPanel.add(coinFaceRadioButtonPanel);
		mainPanel.add(placeBetButton);
		
		add(mainPanel);
		setLocationRelativeTo(this);
		
		// register the JButton with action listeners.
		placeBetButton.addActionListener(placeBetDialogController);
		
		//registerKeyboardAction allows user to interact with JButton using enter/spacebar key
		placeBetButton.registerKeyboardAction(placeBetButton.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		placeBetButton.registerKeyboardAction(placeBetButton.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
	}
	
	public MainView getMainView() 
	{
		return mainView;
	}
	
	public String getBetAmount()
	{
		return betAmountTextField.getText();
	}
	
	public ButtonGroup getButtonGroup()
	{
		return buttonGroup;
	}
}
