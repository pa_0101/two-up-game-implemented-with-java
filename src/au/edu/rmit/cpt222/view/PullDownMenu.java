package au.edu.rmit.cpt222.view;

import java.awt.event.KeyEvent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import au.edu.rmit.cpt222.controller.PullDownMenuController;

/*
 * @author Paolo DiPietro
 * 
 * PullDownMenu class is a control set for the main functions
 * in the game 
 */
@SuppressWarnings("serial")
public class PullDownMenu extends JMenuBar 
{
	private MainView mainView;
	private PullDownMenuController pullDownMenuController;
	
	public PullDownMenu(MainView mainView)
	{
		this.mainView = mainView;
		pullDownMenuController = new PullDownMenuController(this);
	    JMenu menu = new JMenu("Options");
	    JMenuItem addPlayer = new JMenuItem("Add Player");
	    JMenuItem placeBet = new JMenuItem("Place Bet");
	    JMenuItem flipCoins = new JMenuItem("Flip Coins");
	    JMenuItem resetGame = new JMenuItem("Reset Game");
	    JMenuItem setPlayerPoints = new JMenuItem("Set Player Points");
	    JMenuItem displayHistory = new JMenuItem("Display History");
	    JMenuItem exitGame = new JMenuItem("Exit Game");
    
	    // setMnemonic allows user to interact with menu items using enter key
	    menu.setMnemonic(KeyEvent.VK_E); 	    
    
	    menu.add(addPlayer);
	    menu.add(placeBet);
	    menu.add(flipCoins);
	    menu.add(resetGame);
	    menu.add(setPlayerPoints);
	    menu.add(displayHistory);
	    menu.add(exitGame);
	    add(menu);
	    
	    // register the buttons with listeners
	    addPlayer.addActionListener(pullDownMenuController);
		placeBet.addActionListener(pullDownMenuController);
		flipCoins.addActionListener(pullDownMenuController);
		resetGame.addActionListener(pullDownMenuController);
		setPlayerPoints.addActionListener(pullDownMenuController);
		displayHistory.addActionListener(pullDownMenuController);
		exitGame.addActionListener(pullDownMenuController);
	}
	
	public MainView getMainView() 
	{
		return mainView;
	}
}
