package au.edu.rmit.cpt222.view;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/*
 * CoinDisplayArea is responsible for displaying the coin 
 * images
 */
@SuppressWarnings("serial")
public class CoinDisplayArea extends JLabel 
{
	private CoinView coinView;	
	private ImageIcon resizedImageIcon;
	
	public CoinDisplayArea(CoinView coinView)
	{
		this.coinView = coinView;
		setHorizontalAlignment(SwingConstants.CENTER); 
	}
	
	public void setCoinImage(ImageIcon coinImage, int x, int y)
	{
		Image image = coinImage.getImage();
		Image newImage = image.getScaledInstance(160, 160, Image.SCALE_DEFAULT);
		resizedImageIcon = new ImageIcon(newImage);
		resizedImageIcon.paintIcon(this, getGraphics(), x, y);
	}
	
	public void addDisplayArea()
	{
		coinView.addDisplayArea();
	}
	
	public void resetDisplay()
	{
		coinView.removeDisplayArea();
		coinView.addDisplayArea();
	}
}
