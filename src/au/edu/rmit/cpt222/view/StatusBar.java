package au.edu.rmit.cpt222.view;

import javax.swing.JLabel;

/**
 * @author Paolo DiPietro
 * 
 * StatusBar provides feedback to the user for any
 * status/info changes in the game system
 */
@SuppressWarnings("serial")
public class StatusBar extends JLabel 
{
	@SuppressWarnings("unused")
	private MainView mainView;

    public StatusBar(MainView mainView) 
    {
        update(" Game Status:");
    }

	public void update(String gameInfo) 
	{
		setText(gameInfo);
	}  
}
