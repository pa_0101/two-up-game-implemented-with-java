package au.edu.rmit.cpt222.test.ass2;

import java.io.IOException;
import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.view.GameEngineServerView;

/**
 * @author Paolo DiPietro
 * 
 * Server is the main entry point for a creating 
 * and running a game server for all clients to connect to
 */
public class Server 
{
	public static final String SERVER_HOST_NAME = "127.0.0.1";
	public static final int SERVER_PORT = 8000;
	
	public static void main(String[] args) throws IOException 
	{
		GameEngineServerView gameEngineServerView = new GameEngineServerView();
		(new Thread(new GameEngineServerStub(gameEngineServerView, SERVER_HOST_NAME, SERVER_PORT))).start();
	}
}
