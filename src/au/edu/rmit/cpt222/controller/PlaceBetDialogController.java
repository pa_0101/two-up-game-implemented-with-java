package au.edu.rmit.cpt222.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.utilities.Utilities;
import au.edu.rmit.cpt222.view.MainView;
import au.edu.rmit.cpt222.view.PlaceBetDialog;

/**
 * @author Paolo DiPietro
 * 
 * PlaceBetDialogController validates player info input
 * from the user and delegates it to the model.
 */
public class PlaceBetDialogController implements ActionListener 
{
	private Logger logger = Logger.getLogger("assignment2");
	private PlaceBetDialog placeBetDialog;
	@SuppressWarnings("unused")
	private MainView mainView;
	private GameEngine gameEngineClientStub;
	
	public PlaceBetDialogController(PlaceBetDialog placeBetDialog, MainView mainView)
	{
		this.placeBetDialog = placeBetDialog;
		this.mainView = mainView;		
		gameEngineClientStub = MainView.getGameEngine();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		String betAmount = placeBetDialog.getBetAmount();
		// get the group of radio buttons
		ButtonGroup buttonGroup = placeBetDialog.getButtonGroup();
		// get an enum of the buttons in the group
		Enumeration<AbstractButton> jRadioButtons = buttonGroup.getElements();
		JRadioButton currRadioButton;
		String coinFaceSelection = "";
		Coin.Face coinFace = null;
		int betAmountInt = 0;
		
		if(e.getActionCommand().equalsIgnoreCase("Place Bet"))
		{
			if(betAmount.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"Enter a bet amount.");
				return;
			}
			try  
			{  
				// convert input to integer if valid input
				betAmountInt = Integer.parseInt(betAmount);
			}  
			catch(NumberFormatException nfe)  
			{  
				// if input is invalid catch and throw NumberFormatException
				JOptionPane.showMessageDialog(null,"Input must be numeric.");
				return;
		    } 
			
			// iterate through the group of radio buttons
			// and check which one is selected.
			while (jRadioButtons.hasMoreElements()) 
			{
				currRadioButton = (JRadioButton) jRadioButtons.nextElement();
				
				if(currRadioButton.isSelected())
				{
					// get the corresponding text for the button
					coinFaceSelection = currRadioButton.getText();
					
					// assign to the Coin.Face based on the selected button case
					if(coinFaceSelection == "Heads")
					{
						coinFace = Coin.Face.heads;
						break;
					}
					else if(coinFaceSelection == "Tails")
					{
						coinFace = Coin.Face.tails;
						break;
					}
				}				
			}
			
			String playerID = AddPlayerDialogController.getPlayerID();
			Player player = gameEngineClientStub.getPlayer(playerID);

			try 
			{
				gameEngineClientStub.placeBet(player, coinFace, betAmountInt);							
			} 
			catch (InsufficientFundsException e1) 
			{
				Utilities.showMessage("Not enough points to place a bet");
			}						
			
			// refresh the status bar with players betting info
			MainView.refreshStatusBar(gameEngineClientStub.toString(player));	
			logger.log(Level.INFO, gameEngineClientStub.toString(player));
			
			placeBetDialog.setVisible(false);
			
			// dispose() destroys the JDialog window  
			// and is cleaned up by the operating system
			placeBetDialog.dispose();
		}
	}
}
