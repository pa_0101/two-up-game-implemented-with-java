package au.edu.rmit.cpt222.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import au.edu.rmit.cpt222.model.SimplePlayer;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.view.AddPlayerDialog;
import au.edu.rmit.cpt222.view.MainView;

/**
 * @author Paolo DiPietro
 * 
 * AddPlayerDialogController validates player info input
 * from the user and delegates it to the model.
 */
public class AddPlayerDialogController implements ActionListener 
{
	private Logger logger = Logger.getLogger("assignment1");
	private AddPlayerDialog addPlayerDialog;
	@SuppressWarnings("unused")
	private MainView mainView;
	private GameEngine gameEngineClientStub;
	private Player player;
	private static String playerID;
	private String playerName;
	private String creditPoints;
	
	public AddPlayerDialogController(AddPlayerDialog addPlayerDialog, MainView mainView)
	{
		this.addPlayerDialog = addPlayerDialog;
		this.mainView = mainView;
		gameEngineClientStub = MainView.getGameEngine();
	}
		
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		playerID = addPlayerDialog.getPlayerID();
		playerName = addPlayerDialog.getPlayerName();
		creditPoints = addPlayerDialog.getCreditPoints();	
	    int creditPointsInt = 0;
		
	    // check if add player button is pressed and
	    // check for empty input
		if(e.getActionCommand().equalsIgnoreCase("Add Player"))
		{
			if(playerID.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"Enter a player ID.");
				return;
			}
			if(playerName.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"Enter a player name.");
				return;
			}
			if(creditPoints.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"Enter number of credit points.");
				return;
			}
			try  
			{  
				// convert input to integer if valid input
				creditPointsInt = Integer.parseInt(creditPoints);
			}  
			catch(NumberFormatException nfe)  
			{  
				// if input is invalid catch and throw NumberFormatException
				JOptionPane.showMessageDialog(null,"Input must be numeric.");
				return;
		    } 
			
			// create player object and add it to the game
			player = new SimplePlayer(playerID, playerName, creditPointsInt);													
			gameEngineClientStub.addPlayer(player);
			
			// give gameEngineClientStub a record of the player so 
			// we know which player to remove when a client exits the game
			gameEngineClientStub.setCurrentPlayer(player);
			
			// refresh the status bar and logger info
			MainView.refreshStatusBar(gameEngineClientStub.toString(player));		
			logger.log(Level.INFO, gameEngineClientStub.toString(player) + "\n");
			
			addPlayerDialog.setVisible(false);
		
			// dispose() destroys the JDialog window  
			// and is cleaned up by the operating system
			addPlayerDialog.dispose();
		}
	}
	
	static public String getPlayerID()
	{
		return playerID;
	}
}
