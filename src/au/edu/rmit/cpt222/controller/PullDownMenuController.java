package au.edu.rmit.cpt222.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import au.edu.rmit.cpt222.model.GameEngineCallbackImpl;
import au.edu.rmit.cpt222.model.GameOutcomes;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.utilities.Utilities;
import au.edu.rmit.cpt222.view.AddPlayerDialog;
import au.edu.rmit.cpt222.view.CoinView;
import au.edu.rmit.cpt222.view.MainView;
import au.edu.rmit.cpt222.view.PlaceBetDialog;
import au.edu.rmit.cpt222.view.PullDownMenu;
import au.edu.rmit.cpt222.view.SetPlayerPointsDialog;

/**
 * @author Paolo DiPietro
 * 
 * PullDownMenuController validates player info input
 * from the user and delegates it to the model
 */
public class PullDownMenuController implements ActionListener 
{
	private Logger logger = Logger.getLogger("assignment2");
	private PullDownMenu pullDownMenu;
	private GameEngine gameEngineClientStub;
	@SuppressWarnings("unused")
	private MainView mainView;
	private Player player;
	private boolean coinsHaveBeenFliped = false;
	private static GameEngineCallbackImpl gameEngineCallbackImpl;
    private ArrayList<GameOutcomes> gameOutcomesArray;
	
	public PullDownMenuController(PullDownMenu pullDownMenu)
	{
		this.pullDownMenu = pullDownMenu;
		gameEngineClientStub = MainView.getGameEngine();
		mainView = pullDownMenu.getMainView();
		gameEngineCallbackImpl = new GameEngineCallbackImpl();
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
	    // if add player is pressed open the add player dialog
		if(e.getActionCommand().equalsIgnoreCase("Add Player"))
		{
			AddPlayerDialog addPlayerDialog = new AddPlayerDialog(pullDownMenu.getMainView());
			addPlayerDialog.setVisible(true);			
		}
		// if place bet is clicked, check the game state for null
		else if(e.getActionCommand().equalsIgnoreCase("Place Bet"))
		{
			// if game is null or no player added, then display a messgae
			if(gameEngineClientStub == null || gameEngineClientStub.getAllPlayers() == null)
			{
				Utilities.showMessage("Add a player first.");
				return;
			}
			// if a game has already been played, inform the user to reset the game first
			else if(coinsHaveBeenFliped)
			{
				Utilities.showMessage("Reset the game before placing a new bet.");
				return;
			}
			// create and show the place bet dialog
			PlaceBetDialog placeBetDialog = new PlaceBetDialog(pullDownMenu.getMainView());
			placeBetDialog.setVisible(true);
		}
		// check if flip coins is pressed
		else if(e.getActionCommand().equalsIgnoreCase("Flip Coins"))
		{
			String playerID = AddPlayerDialogController.getPlayerID();
			player = gameEngineClientStub.getPlayer(playerID);
						
			// if game is null or no bet placed, then inform the user
			if(gameEngineClientStub.getAllPlayers() == null || gameEngineClientStub.getBet() == 0)
			{
				Utilities.showMessage("Place a bet first.");
				return;
			}
			// if the coins have not been flipped and conditions are met
		    // then call the model to flip the coins
			else if(coinsHaveBeenFliped == false )
			{
				new CoinView();
				gameEngineClientStub.flip(GameEngine.DEFAULT_FLIP_DELAY, GameEngine.DEFAULT_COIN_DELAY);
				
				coinsHaveBeenFliped = true;
			}	
			// check if user has already played
			else if(coinsHaveBeenFliped && player.getPoints() != 0)
			{
				Utilities.showMessage("You have already flipped coins, reset the game and place a bet.");
				return;
			}
			// check if the user has no points left
			else if(coinsHaveBeenFliped && player.getPoints() == 0)
			{
				Utilities.showMessage("You have no more points to play on, exit the game.");
				return;
			}
		}
		// check if reset button is pressed
		else if(e.getActionCommand().equalsIgnoreCase("Reset Game"))
		{
			String playerID = AddPlayerDialogController.getPlayerID();
			player = gameEngineClientStub.getPlayer(playerID);
			
			// if the game is null then inform the user
			if(gameEngineClientStub.getAllPlayers() == null || gameEngineClientStub.getBet() == 0)
			{
				Utilities.showMessage("There is no game to reset.");
				return;
			}
			
            gameEngineClientStub.resetGameStatus();
			
            // get the updated player 
			player = gameEngineClientStub.getPlayer(playerID);
			// update the status bar
			MainView.refreshStatusBar(gameEngineClientStub.toString(player));
			
			Utilities.showMessage("Game/bet is reset.");
			coinsHaveBeenFliped = false;
			
			logger.log(Level.INFO, gameEngineClientStub.toString(player));						
		}
		// check if set player points button is pressed
		else if(e.getActionCommand().equalsIgnoreCase("Set Player Points"))
		{
			// check if a player actually exists to add points to
			if(gameEngineClientStub.getAllPlayers() == null)
			{
				Utilities.showMessage("Add a player with some points first.");
				return;
			}
			// create and show the set player points dialog
			SetPlayerPointsDialog setPlayerPointsDialog = new SetPlayerPointsDialog(pullDownMenu.getMainView());
			setPlayerPointsDialog.setVisible(true);									
		}
		// check if display history button is pressed
		else if(e.getActionCommand().equalsIgnoreCase("Display History"))
		{
			gameOutcomesArray = gameEngineCallbackImpl.getGameOutcomes();			
			
			// check if array is empty 
			if(gameOutcomesArray.size() == 0)
			{
				Utilities.showMessage("There is no collection of player history.");
				return;
			}
			else
			{
				// iterate through all history and display it
				for(GameOutcomes gameOutcome : gameOutcomesArray)
				{
					logger.log(Level.INFO, gameOutcome.getPlayer().getPlayerName() + "'s game history: " + gameOutcome.getResult().toString() + "\n");
				}
				return;
			}
		}
	}
}
