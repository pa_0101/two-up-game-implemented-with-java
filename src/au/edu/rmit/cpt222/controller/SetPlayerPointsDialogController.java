package au.edu.rmit.cpt222.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.view.MainView;
import au.edu.rmit.cpt222.view.SetPlayerPointsDialog;

/**
 * @author Paolo DiPietro
 * 
 * SetPlayerPointsDialogController communicates with the
 * model to set player points during runtime
 * 
 */
public class SetPlayerPointsDialogController implements ActionListener 
{
	private Logger logger = Logger.getLogger("assignment2");
	private SetPlayerPointsDialog setPlayerPointsDialog;
	@SuppressWarnings("unused")
	private MainView mainView;
	private GameEngine gameEngineClientStub;
	private Player player;
	private String creditPoints;
	
	public SetPlayerPointsDialogController(SetPlayerPointsDialog setPlayerPointsDialog, MainView mainView)
	{
		this.setPlayerPointsDialog = setPlayerPointsDialog;
		this.mainView = mainView;		
		gameEngineClientStub = MainView.getGameEngine();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Scanner scanner = new Scanner(System.in);
		creditPoints = setPlayerPointsDialog.getCreditPoints();	
		int creditPointsInt = 0;
				
		if(e.getActionCommand().equalsIgnoreCase("Set Player Points"))
		{
			// check if text field is empty
			if(creditPoints.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"Enter number of credit points.");
				return;
			}
			try  
			{  
				// convert input to integer if valid input
				creditPointsInt = Integer.parseInt(creditPoints);
			}  
			catch(NumberFormatException nfe)  
			{  
				// if input is invalid catch and throw NumberFormatException
				JOptionPane.showMessageDialog(null,"Input must be numeric.");
				return;
		    } 
			
			// get the player and update the points
			String playerID = AddPlayerDialogController.getPlayerID();
			player = gameEngineClientStub.getPlayer(playerID);			  
			gameEngineClientStub.setPlayerPoints(player, creditPointsInt);
					
			// refresh the logger and status bar with the players updated points
			MainView.refreshStatusBar(gameEngineClientStub.toString(player));
			logger.log(Level.INFO, gameEngineClientStub.toString(player) + "\n");
			
			setPlayerPointsDialog.setVisible(false);
					
			// dispose() destroys the JDialog window  
		    // and is cleaned up by the operating system
			setPlayerPointsDialog.dispose();
		}
	}
	
	public Player getPlayer()
	{
		return player;
	}
}
