package au.edu.rmit.cpt222.driver;

import javax.swing.SwingUtilities;

import au.edu.rmit.cpt222.model.comms.GameEngineClientStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.view.MainView;

/**
 * @author Paolo DiPietro
 * 
 * Ass1Driver is the main entry point for a new 
 * game client
 */
public class Ass1Driver
{		
	public static void main(String[] args) 
	{	
		// invokeLater causes run() to be executed asynchronously
		// on the AWT event dispatching thread
		// Runnable is implemented because we need the instances below 
		// to run on separate thread
 		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				// instantiate the main view
				MainView mainView = new MainView();				
				mainView.setVisible(true);
			}
		});	
	}
}
