package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.ObjectOutputStream;

import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class ResetGameStatusCommand extends AbstractRequestCommand 
{

	@Override
	public void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream) 
	{
		GameEngine gameEngineImpl = gameEngineServerStub.getGameEngine();
		gameEngineImpl.resetGameStatus();		
	}
}
