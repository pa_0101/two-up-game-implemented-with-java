package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.IOException;
import java.io.ObjectOutputStream;

import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class GetPlayerCommand extends AbstractRequestCommand 
{
	private String id;
	
	public GetPlayerCommand(String id)
	{
		this.id = id;
	}
	
	@Override
	public void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream) 
	{
		GameEngine gameEngineImpl = gameEngineServerStub.getGameEngine();
		Player player = gameEngineImpl.getPlayer(id);
		
		try 
		{
			requestStream.writeObject(player);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
