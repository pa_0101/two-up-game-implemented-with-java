package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.IOException;
import java.io.ObjectOutputStream;
import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class GetBetCommand extends AbstractRequestCommand 
{
	@Override
	public void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream) 
	{
		GameEngine gameEngineImpl = gameEngineServerStub.getGameEngine();
		int betAmount = gameEngineImpl.getBet();
		
		try 
		{
			requestStream.writeObject(betAmount);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
