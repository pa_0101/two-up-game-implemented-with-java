package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.ObjectOutputStream;
import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class PlaceBetCommand extends AbstractRequestCommand 
{
	private Player player;
	private Face face;
	private int bet;
	
	public PlaceBetCommand(Player player, Face face, int bet)
	{
		this.player = player;
		this.face = face;
		this.bet = bet;
	}
	
	@Override
	public void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream) 
	{
		GameEngine gameEngineImpl = gameEngineServerStub.getGameEngine();
		try 
		{
			gameEngineImpl.placeBet(player, face, bet);
		} 
		catch (InsufficientFundsException e) 
		{
			e.printStackTrace();
		}
	}
}
