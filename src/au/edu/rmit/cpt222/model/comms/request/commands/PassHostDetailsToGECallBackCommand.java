package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.ObjectOutputStream;
import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.utilities.HostDetails;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class PassHostDetailsToGECallBackCommand extends AbstractRequestCommand 
{
	private HostDetails hostDetails;
	
	public PassHostDetailsToGECallBackCommand(HostDetails hostDetails)
	{
		this.hostDetails = hostDetails;
	}
	
	@Override
	public void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream) 
	{
		gameEngineServerStub.addGameEngineCallBack(hostDetails);		
	}
}
