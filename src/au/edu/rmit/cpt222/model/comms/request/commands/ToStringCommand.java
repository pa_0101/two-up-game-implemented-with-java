package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.IOException;
import java.io.ObjectOutputStream;

import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class ToStringCommand extends AbstractRequestCommand 
{
	private Player player;
	
	public ToStringCommand(Player player)
	{
		this.player = player;
    }
	
	@Override
	public void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream) 
	{		
		GameEngine gameEngineImpl = gameEngineServerStub.getGameEngine();
		String statusString = gameEngineImpl.toString(player);
		
		try 
		{
			requestStream.writeObject(statusString);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
