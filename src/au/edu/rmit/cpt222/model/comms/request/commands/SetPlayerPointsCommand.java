package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.ObjectOutputStream;

import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class SetPlayerPointsCommand extends AbstractRequestCommand 
{
	private Player player;
	private int totalPoints;
	
	public SetPlayerPointsCommand(Player player, int totalPoints)
	{		
		this.player = player;
		this.totalPoints = totalPoints;
	}
	
	@Override
	public void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream) 
	{
		GameEngine gameEngineImpl = gameEngineServerStub.getGameEngine();
		gameEngineImpl.setPlayerPoints(player, totalPoints);
	}
}
