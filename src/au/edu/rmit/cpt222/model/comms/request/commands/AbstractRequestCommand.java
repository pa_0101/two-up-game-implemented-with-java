package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public abstract class AbstractRequestCommand implements Serializable 
{	
	public abstract void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream);
}
