package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collection;

import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class GetAllPlayersCommand extends AbstractRequestCommand 
{
	public GetAllPlayersCommand()
	{
		
	}
	
	@Override
	public void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream) 
	{
		GameEngine gameEngineImpl = gameEngineServerStub.getGameEngine();
		Collection<Player> players = gameEngineImpl.getAllPlayers();
		
		try 
		{
			requestStream.writeObject(players);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
