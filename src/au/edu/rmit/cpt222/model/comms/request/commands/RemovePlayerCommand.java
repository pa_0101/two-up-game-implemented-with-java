package au.edu.rmit.cpt222.model.comms.request.commands;

import java.io.IOException;
import java.io.ObjectOutputStream;
import au.edu.rmit.cpt222.model.comms.GameEngineServerStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class RemovePlayerCommand extends AbstractRequestCommand 
{
	private Player player;
	
	public RemovePlayerCommand(Player player)
	{
		this.player = player;
	}
	
	@Override
	public void executeRequest(GameEngineServerStub gameEngineServerStub, ObjectOutputStream requestStream) 
	{		
		GameEngine gameEngineImpl = gameEngineServerStub.getGameEngine();
		boolean result = gameEngineImpl.removePlayer(player);
		
		try 
		{
			requestStream.writeObject(result);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
