package au.edu.rmit.cpt222.model.comms;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import au.edu.rmit.cpt222.model.GameEngineImpl;
import au.edu.rmit.cpt222.model.comms.callback.ServerStubGameEngineCallBack;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.utilities.HostDetails;
import au.edu.rmit.cpt222.view.GameEngineServerView;

/**
 * @author Paolo DiPietro
 * 
 * GameEngineServerStub handles any incoming clients that 
 * want to connect to the server.
 */
public class GameEngineServerStub implements Runnable
{	
	private ServerSocket serverSocket;
	private Socket socket;
	private GameEngine gameEngine;
	private GameEngineServerView gameEngineServerView;
	private String hostName;
	
	public GameEngineServerStub(GameEngineServerView gameEngineServerView, String hostName, int port) throws IOException
	{	
		this.gameEngineServerView = gameEngineServerView;
		this.hostName = hostName;
		
		gameEngine = new GameEngineImpl();
		
		// Create a server socket
		try
		{			
			// create a server socket
			serverSocket = new ServerSocket(port);	
			// print a message to GUI saying that server has started
			gameEngineServerView.printToServerGUI("Multi Player Server started at " + new Date() + '\n');
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	
	}

	@Override
	public void run() 
	{
		int clientCount = 1;
		
		while(!serverSocket.isClosed())
		{				
			// Listen for a connection request and accept it 
			// if valid
			try 
			{   
				socket = serverSocket.accept();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
			
			// print a message on the GUI saying that
			// client has connected
			gameEngineServerView.printToServerGUI("Starting thread for client " + clientCount + " at " + new Date() + "\n");
			gameEngineServerView.printToServerGUI("Client " + clientCount + " host address is " + hostName + "\n");
			
			// create a new RequestTaskHandler thread for any incoming client commands
			(new Thread(new RequestTaskHandler(this, socket))).start();	
			// imcrement the number of clients connected to the server
			clientCount++;
		}
	}
	
	public GameEngine getGameEngine() 
	{
		return gameEngine;
	}
	
	/*
	 * Add a server side game engine call back 
	 * for all clients to be able to communicate back 
	 * to their corresponding client side call back logic
	 */
	public void addGameEngineCallBack(HostDetails hostDetails)
	{
		gameEngine.addGameEngineCallback(new ServerStubGameEngineCallBack(hostDetails));
	}
}
