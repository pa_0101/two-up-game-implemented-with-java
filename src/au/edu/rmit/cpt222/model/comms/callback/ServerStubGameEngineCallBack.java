package au.edu.rmit.cpt222.model.comms.callback;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import au.edu.rmit.cpt222.model.comms.response.commands.CoinFlipCommand;
import au.edu.rmit.cpt222.model.comms.response.commands.CoinFlipOutcomeCommand;
import au.edu.rmit.cpt222.model.comms.response.commands.GameResultCommand;
import au.edu.rmit.cpt222.model.comms.response.commands.SetWhichCoinIsFlippingCommand;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.CoinFlip;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.utilities.HostDetails;

/**
 * @author Paolo DiPietro
 * 
 * ServerStubGameEngineCallBack creates all command objects that 
 * correspond to a method of the game engine callback 
 * on the client side. These objects are 
 * serialized to an output stream to sent through the pipeline
 * to the client side
 */
public class ServerStubGameEngineCallBack implements GameEngineCallback
{
	private Socket socket;	
	private ObjectOutputStream requestStream;
	private ObjectInputStream responseStream;	 
	
	public ServerStubGameEngineCallBack(HostDetails hostDetails)
	{
		try 
		{			
			socket = new Socket(hostDetails.getHostName(), hostDetails.getPort());
			// setup the socket streams
			requestStream = new ObjectOutputStream(socket.getOutputStream());
			requestStream.flush();
			responseStream = new ObjectInputStream(socket.getInputStream());
		} 
		catch (IOException e) 
		{			
			if (socket != null)
			{
				try 
				{
					socket.close();
				} 
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngineCallback#coinFlip(au.edu.rmit.cpt222.model.interfaces.Coin.Face, 
	 * au.edu.rmit.cpt222.model.interfaces.GameEngine)
	 */
	@Override
	public void coinFlip(Face coinFace, GameEngine engine) 
	{	
		try 
		{
			requestStream.writeObject(new CoinFlipCommand(coinFace, engine));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngineCallback#coinFlipOutcome(au.edu.rmit.cpt222.model.interfaces.Coin.Face, 
	 * au.edu.rmit.cpt222.model.interfaces.GameEngine)
	 */
	@Override
	public void coinFlipOutcome(Face coinFace, GameEngine engine) 
	{
		try 
		{
			requestStream.writeObject(new CoinFlipOutcomeCommand(coinFace, engine));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngineCallback#gameResult(au.edu.rmit.cpt222.model.interfaces.Player, 
	 * au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus, au.edu.rmit.cpt222.model.interfaces.GameEngine)
	 */
	@Override
	public void gameResult(Player player, GameStatus result, GameEngine engine) 
	{
		try 
		{					
			requestStream.writeObject(new GameResultCommand(player, result, engine));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngineCallback#setWhichCoinIsFlipping(au.edu.rmit.cpt222.model.interfaces.GameEngine.CoinFlip)
	 */
	@Override
	public void setWhichCoinIsFlipping(CoinFlip currFlippingCoin) 
	{ 
		try 
		{
			requestStream.writeObject(new SetWhichCoinIsFlippingCommand(currFlippingCoin));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
