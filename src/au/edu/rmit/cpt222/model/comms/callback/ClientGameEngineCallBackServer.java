package au.edu.rmit.cpt222.model.comms.callback;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import au.edu.rmit.cpt222.model.GameEngineCallbackImpl;
import au.edu.rmit.cpt222.model.comms.GameEngineClientStub;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.utilities.HostDetails;

/*
 * ClientGameEngineCallBackServer is responsible for receiving
 * callbacks from the server side
 */
public class ClientGameEngineCallBackServer implements Runnable
{
	private GameEngineClientStub gameEngineClientStub;
	private ServerSocket serverSocket;
	@SuppressWarnings("unused")
	private GameEngine ameEngineClientStub;
	private Socket socket;
	private HostDetails hostDetails;
	private GameEngineCallback gameEngineCallback;
	private String localhost = "127.0.0.1";
	
	public ClientGameEngineCallBackServer(GameEngineClientStub gameEngineClientStub) 
	{		
		this.gameEngineClientStub = gameEngineClientStub; 
		gameEngineCallback = new GameEngineCallbackImpl();
				
		try 
		{
			// create a socket with an automatically generated port number
			serverSocket = new ServerSocket(0);
		} 
		catch (IOException e) 
		{		
			e.printStackTrace();
		}
		
		// create a host details for each client
		hostDetails = new HostDetails(localhost, serverSocket.getLocalPort());
	}
	
    @Override
    public void run()
    {            	
    	try 
	    {	
    		gameEngineClientStub.passHostDetailsToGECallBack(hostDetails);
			
			while(!serverSocket.isClosed())
			{
				// listen for a connection request
				socket = serverSocket.accept();
				
				// create a new call back handler thread
				(new Thread(new ResponseTaskHandler(this, socket))).start();
			}
		} 
	    catch (IOException ex)
		{
			if (serverSocket != null)
			{
				try 
				{
					serverSocket.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
			System.err.println(ex);
		}	            	
    } 
	
	public HostDetails getHostDetails()
	{	
		return hostDetails;
	}
	
	public GameEngineCallback getCallBack()
	{
		return gameEngineCallback;
	}
}
