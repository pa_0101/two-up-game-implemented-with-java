package au.edu.rmit.cpt222.model.comms.callback;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import au.edu.rmit.cpt222.model.comms.response.commands.AbstractResponseCommand;

/**
 * @author Paolo DiPietro
 * 
 * ResponseTaskHandler handles any incoming serialized object
 * streams which are then cast back to their original form.
 * The methods are then executed on the cliet side game engine
 * call back
 */
public class ResponseTaskHandler implements Runnable 
{
	private AbstractResponseCommand command;
	private ClientGameEngineCallBackServer clientGameEngineCallBackServer;
	private Socket socket;
	
	public ResponseTaskHandler(ClientGameEngineCallBackServer clientGameEngineCallBackServer, Socket socket)
	{
		this.clientGameEngineCallBackServer = clientGameEngineCallBackServer;
		this.socket = socket;
	}
	
	public void run()
	{	
		try
		{
			// Create data input and output streams
			ObjectInputStream responseStream = new ObjectInputStream(socket.getInputStream());
			ObjectOutputStream requestStream = new ObjectOutputStream(socket.getOutputStream());
			
			while (true)
			{				
				try 
				{		
					// read the stream and cast it back to its object
					command = (AbstractResponseCommand)responseStream.readObject();
					// execute the command on the client side
					command.executeResponse(clientGameEngineCallBackServer, requestStream);
				} 
				catch (ClassNotFoundException e) 
				{
					e.printStackTrace();
				}
			}
		} 
		catch (IOException ex)
		{
			System.err.println(ex);
		}
	}	
}
