package au.edu.rmit.cpt222.model.comms;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Collection;
import au.edu.rmit.cpt222.model.comms.callback.ClientGameEngineCallBackServer;
import au.edu.rmit.cpt222.model.comms.request.commands.AddPlayerCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.CalculateResultCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.FlipCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.GetAllPlayersCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.GetBetCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.GetPlayerCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.PassHostDetailsToGECallBackCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.PlaceBetCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.RemovePlayerCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.ResetGameStatusCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.SetPlayerPointsCommand;
import au.edu.rmit.cpt222.model.comms.request.commands.ToStringCommand;
import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.utilities.HostDetails;

/**
 * @author Paolo DiPietro
 * 
 * GameEngineClientStub creates all command objects that 
 * correspond to a method of the game engine. These objects are 
 * serialized to an output stream to sent through the pipeline
 * to the server side
 */
public class GameEngineClientStub implements GameEngine 
{
	private ObjectOutputStream requestStream;
	private ObjectInputStream responseStream;	
	private Socket clientSocket;
	public static final String SERVER_HOST_NAME = "127.0.0.1";
	public static final int SERVER_PORT = 8000;	
	private Player currentPlayer;
	
	public GameEngineClientStub() 
	{
		try 
		{		
			// create a new socket
			clientSocket = new Socket(SERVER_HOST_NAME, SERVER_PORT);
			// setup the socket streams
			requestStream = new ObjectOutputStream(clientSocket.getOutputStream());
			requestStream.flush();
			responseStream = new ObjectInputStream(clientSocket.getInputStream());
		} 
		catch (IOException e) 
		{			
			if (clientSocket != null)
			{
				try 
				{
					clientSocket.close();
				} 
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
		}
		(new Thread(new ClientGameEngineCallBackServer(this))).start();
	}
	
	/*
	 * the host details of the callBack server needs to be passed later to
	 * the server stub so that it can connect to it later (to pass callbacks)
	 */
	public void passHostDetailsToGECallBack(HostDetails hostDetails)
	{
		try 
		{			
			requestStream.writeObject(new PassHostDetailsToGECallBackCommand(hostDetails));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#addGameEngineCallback(au.edu.rmit.cpt222.model.interfaces.GameEngineCallback)
	 */
	@Override
	public void addGameEngineCallback(GameEngineCallback gameEngineCallback) {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#addPlayer(au.edu.rmit.cpt222.model.interfaces.Player)
	 */
	@Override
	public void addPlayer(Player player)
	{
		try 
		{
			requestStream.writeObject(new AddPlayerCommand(player));
		} 
		catch (IOException e) 
		{
			e.printStackTrace(); 
		}	
	}   

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#calculateResult()
	 */
	@Override
	public void calculateResult() 
	{
		try 
		{
			requestStream.writeObject(new CalculateResultCommand());
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#flip(int, int)
	 */
	@Override
	public void flip(int flipDelay, int coinDelay) 
	{
		try 
		{
			requestStream.writeObject(new FlipCommand(flipDelay, coinDelay));
			calculateResult();	
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#getAllPlayers()
	 */	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Player> getAllPlayers() 
	{
		try 
		{
			requestStream.writeObject(new GetAllPlayersCommand());
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		try 
		{
			return (Collection<Player>) responseStream.readObject();
		} 
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#getPlayer(java.lang.String)
	 */
	@Override
	public Player getPlayer(String id) 
	{
		try 
		{
			requestStream.writeObject(new GetPlayerCommand(id));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}		
		
		try 
		{
			return (Player) responseStream.readObject();
		} 
		catch (ClassNotFoundException e) 
		{		
			e.printStackTrace();
		} 
		catch (IOException e) 
		{			
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#placeBet(au.edu.rmit.cpt222.model.interfaces.Player, au.edu.rmit.cpt222.model.interfaces.Coin.Face, int)
	 */
	@Override
	public void placeBet(Player player, Face face, int bet) throws InsufficientFundsException 
	{
		try 
		{
			requestStream.writeObject(new PlaceBetCommand(player, face, bet));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#removeGameEngineCallback(au.edu.rmit.cpt222.model.interfaces.GameEngineCallback)
	 */
	@Override
	public void removeGameEngineCallback(GameEngineCallback gameEngineCallback) {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#removePlayer(au.edu.rmit.cpt222.model.interfaces.Player)
	 */
	@Override
	public boolean removePlayer(Player player) 
	{
		try 
		{
			requestStream.writeObject(new RemovePlayerCommand(player));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}		
		
		try 
		{
			return (boolean) responseStream.readObject();
		} 
		catch (ClassNotFoundException e) 
		{		
			e.printStackTrace();
		} 
		catch (IOException e) 
		{			
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#setPlayerPoints(au.edu.rmit.cpt222.model.interfaces.Player, int)
	 */
	@Override
	public void setPlayerPoints(Player player, int totalPoints) 
	{		
		try 
		{
			requestStream.writeObject(new SetPlayerPointsCommand(player, totalPoints));			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#toString(au.edu.rmit.cpt222.model.interfaces.Player)
	 */
	@Override
	public String toString(Player player) 
	{
		try 
		{
			requestStream.writeObject(new ToStringCommand(player));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}		
		
		try 
		{
			return (String) responseStream.readObject();
		} 
		catch (ClassNotFoundException e) 
		{		
			e.printStackTrace();
		} 
		catch (IOException e) 
		{			
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#getBet()
	 */	
	@Override
	public int getBet()
	{
		try 
		{
			requestStream.writeObject(new GetBetCommand());
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}		
		
		try 
		{
			return (int) responseStream.readObject();
		} 
		catch (ClassNotFoundException e) 
		{		
			e.printStackTrace();
		} 
		catch (IOException e) 
		{			
			e.printStackTrace();
		}
		return 0;	
	}
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#resetGameStatus()
	 */
	@Override
	public void resetGameStatus()
	{
		try 
		{
			requestStream.writeObject(new ResetGameStatusCommand());
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}		
	}
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#setCurrentPlayer(au.edu.rmit.cpt222.model.interfaces.Player)
	 */
	@Override
	public void setCurrentPlayer(Player player)
	{
		currentPlayer = player;
	}
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#getCurrentPlayer()
	 */
	@Override
	public Player getCurrentPlayer()
	{
		return currentPlayer;
	}
}
