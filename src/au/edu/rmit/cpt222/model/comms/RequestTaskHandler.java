package au.edu.rmit.cpt222.model.comms;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import au.edu.rmit.cpt222.model.comms.request.commands.AbstractRequestCommand;
     
/**
 * @author Paolo DiPietro
 * 
 * RequestTaskHandler handles any incoming serialized object
 * streams which are then cast back to their original form.
 * The methods are then executed on the server side game engine
 */
public class RequestTaskHandler extends Thread 
{	
	private AbstractRequestCommand command;
	private GameEngineServerStub gameEngineServerStub;
	private Socket socket;

	public RequestTaskHandler(GameEngineServerStub gameEngineServerStub, Socket socket)
	{		
		this.gameEngineServerStub = gameEngineServerStub;
		this.socket = socket;
	}

	public void run()
	{	
		try
		{
			// Create data input and output streams
			ObjectInputStream responseStream = new ObjectInputStream(socket.getInputStream());
			ObjectOutputStream requestStream = new ObjectOutputStream(socket.getOutputStream());
			
			while (true)
			{
				try 
				{
					// read the stream and cast it back to its object
					command = (AbstractRequestCommand)responseStream.readObject();
					// execute the command on the server side
					command.executeRequest(gameEngineServerStub, requestStream);
				} 
				catch (ClassNotFoundException e) 
				{
					e.printStackTrace();
				}
			}
		} 
		catch (IOException ex)
		{
			System.err.println(ex);
		}
	}	
}
