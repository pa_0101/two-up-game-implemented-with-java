package au.edu.rmit.cpt222.model.comms.response.commands;

import java.io.ObjectOutputStream;

import au.edu.rmit.cpt222.model.comms.callback.ClientGameEngineCallBackServer;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class GameResultCommand extends AbstractResponseCommand 
{
	private Player player; 
	private GameStatus result;
	private transient GameEngine engine;
	
	public GameResultCommand(Player player, GameStatus result, GameEngine engine)
	{
		this.player = player;
		this.result = result;
		this.engine = engine;
	}
	
	@Override
	public void executeResponse(ClientGameEngineCallBackServer clientGameEngineCallBackServer, ObjectOutputStream requestStream) 
	{
		GameEngineCallback gameEngineCallbackImpl = clientGameEngineCallBackServer.getCallBack();
		gameEngineCallbackImpl.gameResult(player, result, engine);
	}
}
