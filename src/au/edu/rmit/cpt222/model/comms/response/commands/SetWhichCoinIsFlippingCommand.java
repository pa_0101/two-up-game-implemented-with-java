package au.edu.rmit.cpt222.model.comms.response.commands;

import java.io.ObjectOutputStream;

import au.edu.rmit.cpt222.model.comms.callback.ClientGameEngineCallBackServer;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.CoinFlip;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class SetWhichCoinIsFlippingCommand extends AbstractResponseCommand 
{
	private CoinFlip currFlippingCoin;
	
	public SetWhichCoinIsFlippingCommand(CoinFlip currFlippingCoin)
	{
		this.currFlippingCoin = currFlippingCoin;
	}
	
	@Override
	public void executeResponse(ClientGameEngineCallBackServer clientGameEngineCallBackServer, ObjectOutputStream requestStream) 
	{
		GameEngineCallback gameEngineCallbackImpl = clientGameEngineCallBackServer.getCallBack();
		gameEngineCallbackImpl.setWhichCoinIsFlipping(currFlippingCoin);
	}
}
