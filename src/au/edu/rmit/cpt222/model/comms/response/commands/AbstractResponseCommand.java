package au.edu.rmit.cpt222.model.comms.response.commands;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import au.edu.rmit.cpt222.model.comms.callback.ClientGameEngineCallBackServer;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public abstract class AbstractResponseCommand implements Serializable 
{
	public abstract void executeResponse(ClientGameEngineCallBackServer clientGameEngineCallBackServer, ObjectOutputStream requestStream);
}
