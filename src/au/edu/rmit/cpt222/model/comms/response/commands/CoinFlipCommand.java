package au.edu.rmit.cpt222.model.comms.response.commands;

import java.io.ObjectOutputStream;

import au.edu.rmit.cpt222.model.comms.callback.ClientGameEngineCallBackServer;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;

/**
 * @author Paolo DiPietro
 */
@SuppressWarnings("serial")
public class CoinFlipCommand extends AbstractResponseCommand 
{
	private Face coinFace; 
	private transient GameEngine gameEngine;
	
	public CoinFlipCommand(Face coinFace, GameEngine gameEngine)
	{
		this.coinFace = coinFace;
		this.gameEngine = gameEngine;
	}
	
	@Override
	public void executeResponse(ClientGameEngineCallBackServer clientGameEngineCallBackServer,ObjectOutputStream requestStream) 
	{
		GameEngineCallback gameEngineCallbackImpl = clientGameEngineCallBackServer.getCallBack();		
		gameEngineCallbackImpl.coinFlip(coinFace, gameEngine);
	}
}
