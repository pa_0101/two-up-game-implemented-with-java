package au.edu.rmit.cpt222.model;

import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;
import au.edu.rmit.cpt222.model.interfaces.Player;

/**
 * @author Paolo DiPietro
 * 
 * SimplePlayer provides the implementation
 * of the Player interface.
 */
@SuppressWarnings("serial")
public class SimplePlayer implements Player 
{
	private String playerID;
	private String playerName;
	private int defaultCreditPoints;
	private static int betAmount;
	private static Face facePick;
	private GameEngine.GameStatus gameResult;
	
	public SimplePlayer(String playerID, String playerName, int defaultCreditPoints) 
	{	
		this.playerID = playerID;
		this.playerName = playerName;
		this.defaultCreditPoints = defaultCreditPoints;
	}
	
	@Override
	public int getBet() 
	{
		return betAmount;
	}
	
	@Override
	public Face getFacePick() 
	{
		return facePick;
	}

	@Override
	public String getPlayerId() 
	{
		return playerID;
	}

	@Override
	public String getPlayerName() 
	{
		return playerName;
	}

	@Override
	public int getPoints() 
	{
		return defaultCreditPoints;
	}

	@Override
	public GameStatus getResult() 
	{
		return gameResult;
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.Player#placeBet(au.edu.rmit.cpt222.model.interfaces.Coin.Face, int)
	 */
	@Override
	public synchronized void placeBet(Face facePick, int bet)
			throws InsufficientFundsException 
	{
		betAmount = bet;
		SimplePlayer.facePick = facePick;
		
		// if the players bet is larger than their
		// current amount of points, thrown InsufficientFundsException
		if(bet > defaultCreditPoints) 
		{				
			throw new InsufficientFundsException("Not enough points to place a bet.");
		}
		// if face has not been picked or bet is 0 or less,
		// throw AssertionError
		if(facePick == null || bet < 1)
		{
			throw new AssertionError();
		}
	}

	@Override
	public void setPlayerName(String playerName) 
	{
		this.playerName = playerName;
	}

	@Override
	public synchronized void setPoints(int points) 
	{
		defaultCreditPoints = points;
	}

	@Override
	public void setResult(GameStatus status) 
	{
		gameResult = status;
	}
	
	@Override
	public synchronized String toString() 
	{			
		return "   Player ID: " + playerID + " | Player Name: " + playerName + " | Bet Amount: " + betAmount
			 + " | Selected Coin Face: " + facePick + " | Game Result: " + gameResult
			 + " | Total Points: " + defaultCreditPoints + "\n";  
	}
	
	@Override
	public void resetGameStatus()
	{
		betAmount = 0;
		facePick = null;
		gameResult = null;
	}
}
