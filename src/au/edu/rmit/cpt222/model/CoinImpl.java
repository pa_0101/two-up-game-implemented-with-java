package au.edu.rmit.cpt222.model;

import java.awt.Image;
import javax.swing.ImageIcon;
import au.edu.rmit.cpt222.model.interfaces.Coin;

/**
 * @author Paolo DiPietro
 * 
 * CoinImpl provides the implementation of the 
 * Coin interface.
 */
public class CoinImpl implements Coin 
{
	private Coin.Face coinFace; 
	private ImageIcon head;
	private ImageIcon tail;
	private Image headImg;
	private Image tailImg; 
	private Image headInstance; 
	private Image tailInstance; 
	private ImageIcon coinHead;
	private ImageIcon tailHead;
	
	public CoinImpl()
	{
		head = new ImageIcon("assets/head.png");
		tail = new ImageIcon("assets/tail.png");
		headImg = head.getImage();
		tailImg = tail.getImage();
		headInstance = headImg.getScaledInstance(300, 300, java.awt.Image.SCALE_SMOOTH);
		tailInstance = tailImg.getScaledInstance(300, 300, java.awt.Image.SCALE_SMOOTH);
		coinHead = new ImageIcon(headInstance);
		tailHead = new ImageIcon(tailInstance);
	}
	
	@Override
	public Face getCurrentFace() 
	{
		return coinFace;
	}

	@Override
	public void setCurrentFace(Face currentFace) 
	{
		this.coinFace = currentFace;	
	}

	@Override
	public ImageIcon swapFace() 
	{
		if(coinFace == Coin.Face.heads)
		{	
			return coinHead;
		}
		else if(coinFace == Coin.Face.tails)
		{
			return tailHead;
		}		
		return null;
	}  
}
