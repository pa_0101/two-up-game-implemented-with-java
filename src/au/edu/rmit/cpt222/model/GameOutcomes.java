package au.edu.rmit.cpt222.model;

import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;
import au.edu.rmit.cpt222.model.interfaces.Player;

/*
 * @author Paolo DiPietro
 * GameOutcomes encapsulate the players game results
 * which are stored in list to be viewed in the console
 */
public class GameOutcomes 
{
	private GameStatus result;	
	private Player player;
	
	public GameOutcomes(GameStatus result, Player player) 
	{
		this.result = result;
		this.player = player;
	}

	public GameStatus getResult() 
	{
		return result;
	}

	public Player getPlayer() 
	{
		return player;
	}
}
