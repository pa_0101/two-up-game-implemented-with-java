package au.edu.rmit.cpt222.model.interfaces;

import javax.swing.ImageIcon;

/**
 * Assignment interface for SADI representing the coin.
 * 
 * @author Mikhail Perepletchikov
 * 
 */
public interface Coin {

	public enum Face {
		heads, tails
	}

	/**
	 * @return the "selected" face of this coin based on {@link Face}
	 */
	public Face getCurrentFace();

	/**
	 * @param currentFace
	 *            current face of the coin
	 */
	public void setCurrentFace(Face currentFace);

	/**
	 * Changes (swaps) the coin faces.
	 * 
	 * @return the swapped coin face image
	 */
	public ImageIcon swapFace();
}
