package au.edu.rmit.cpt222.model;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;

import au.edu.rmit.cpt222.model.comms.GameEngineClientStub;
import au.edu.rmit.cpt222.model.interfaces.*;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;
import au.edu.rmit.cpt222.view.CoinView;
import au.edu.rmit.cpt222.view.CoinDisplayArea;
import au.edu.rmit.cpt222.view.MainView;

/**
 * @author Paolo DiPietro
 * 
 * GameEngineCallbackImpl provides the implementation of
 * the GameEngineCallback interface.
 */
public class GameEngineCallbackImpl implements GameEngineCallback 
{		
	private Logger logger = Logger.getLogger("assignment1");
	private Coin coin = new CoinImpl();
	private ImageIcon coinImage;
	private CoinDisplayArea displayArea;
	private GameEngine.CoinFlip currFlippingCoin;
	private int maxNumberOfGameOutcomes = 5;
	private static ArrayList<GameOutcomes> gameResults = new ArrayList<GameOutcomes>();
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngineCallback#coinFlip(au.edu.rmit.cpt222.model.interfaces.Coin.Face, 
	 * au.edu.rmit.cpt222.model.interfaces.GameEngine)
	 */
	@Override
	public void coinFlip(Face coinFace, GameEngine engine) 
	{
		displayArea = CoinView.getDisplayArea();
		
		if(displayArea != null)
		{
			// check for the first coin flip
			if(currFlippingCoin == GameEngine.CoinFlip.COINFLIP_1)
			{
				coin.setCurrentFace(coinFace);
				coinImage = coin.swapFace();
				
				// set the coin image at the x,y input coordinates
				displayArea.setCoinImage(coinImage, 30, 12);
				displayArea.addDisplayArea();
			}
			// check for the second coin flip
			else if(currFlippingCoin == GameEngine.CoinFlip.COINFLIP_2)
			{
				coin.setCurrentFace(coinFace);
				coinImage = coin.swapFace();
				
				// set the coin image at the x,y input coordinates
				displayArea.setCoinImage(coinImage, 210, 12);
				displayArea.addDisplayArea();
			}
		}
		logger.log(Level.INFO, coinFace.toString());	
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngineCallback#coinFlipOutcome(au.edu.rmit.cpt222.model.interfaces.Coin.Face, 
	 * au.edu.rmit.cpt222.model.interfaces.GameEngine)
	 */
	@Override
	public void coinFlipOutcome(Face coinFace, GameEngine engine) 
	{	
		coin.setCurrentFace(coinFace);
		
		logger.log(Level.INFO, "Final coin face= " + coinFace.toString() + "\n");
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngineCallback#gameResult(au.edu.rmit.cpt222.model.interfaces.Player, 
	 * au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus, au.edu.rmit.cpt222.model.interfaces.GameEngine)
	 */
	@Override
	public void gameResult(Player player, GameStatus result, GameEngine engine) 
	{	
		// check if list has reached 5 stored game results
		if(gameResults.size() != maxNumberOfGameOutcomes)
		{
			// create a game outcome object
			// and add it to the list
			GameOutcomes gameOutcomes = new GameOutcomes(result, player); 
			gameResults.add(gameOutcomes);
		}
		engine = MainView.getGameEngine();
		
		// update the status bar and logger with player and results info
		MainView.refreshStatusBar(engine.toString(player));
		logger.log(Level.INFO, "Player: " + player.getPlayerName() + " has " + result.toString() + "\n");
	}
	
	/*
	 * Return the the game results list
	 */
	public ArrayList<GameOutcomes> getGameOutcomes()
	{
		return gameResults;
	}
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngineCallback#setWhichCoinIsFlipping(au.edu.rmit.cpt222.model.interfaces.GameEngine.CoinFlip)
	 */
	@Override
	public void setWhichCoinIsFlipping(GameEngine.CoinFlip currFlippingCoin)
	{
		this.currFlippingCoin = currFlippingCoin;
	}
}
