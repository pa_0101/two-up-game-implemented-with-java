package au.edu.rmit.cpt222.model;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Random;

import au.edu.rmit.cpt222.model.comms.callback.ServerStubGameEngineCallBack;
import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.utilities.HostDetails;
import au.edu.rmit.cpt222.utilities.Utilities;

/**
 * @author Paolo DiPietro
 * 
 * GameEngineImpl provides the implementation of 
 * the GameEngine interface.
 */
public class GameEngineImpl extends Thread implements GameEngine 
{
	private GameEngineCallback gameEngineCallback;
	private ArrayList<Player> players = new ArrayList<Player>();	
	private ArrayList<Coin.Face> coinFaces = new ArrayList<Coin.Face>();
	private Player player;	
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#addGameEngineCallback(au.edu.rmit.cpt222.model.interfaces.GameEngineCallback)
	 */
	@Override
	public void addGameEngineCallback(GameEngineCallback gameEngineCallback) 
	{
		this.gameEngineCallback = gameEngineCallback;
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#addPlayer(au.edu.rmit.cpt222.model.interfaces.Player)
	 */
	@Override
	public synchronized void addPlayer(Player player) 
	{	
		this.player = player;
		/*System.out.println("GAME ENGINE IMPL addPlayer" +
						   "\nPLAYER NAME: " + player.getPlayerName() + 
						   "\nPLAYER ID: " + player.getPlayerId() + 
						   "\nAMOUNT OF POINTS: " + player.getPoints());*/
		players.add(player);
	}
		
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#flip(int, int)
	 */
	@Override
	public synchronized void flip(int flipDelay, int coinDelay) throws IllegalArgumentException 
	{	
		// throw an exception if flip and coin delays are
				// invalid
		if(flipDelay != GameEngine.DEFAULT_FLIP_DELAY || coinDelay != GameEngine.DEFAULT_COIN_DELAY)
		{
			throw new IllegalArgumentException();
		}
		// do 2 iterations, 1 for each flip of a coin
		for(int i = 0; i < 2; i++)
		{
			// set which coin is being flipped
			if(i == 0)
			{
				gameEngineCallback.setWhichCoinIsFlipping(GameEngine.CoinFlip.COINFLIP_1);
			}
			else if(i == 1)
			{
				gameEngineCallback.setWhichCoinIsFlipping(GameEngine.CoinFlip.COINFLIP_2);
			}
			// randomNum controls how many times the loop ticks over 
			// to call the coinFlip method. This is to try and simulate
			// a real life coin flip.
			int randomNum = generateRandomNumberInRange();
			boolean isTails = true;	
			
			// for each random number of iterations, 
			// call the coin flip method
			for(int j = 0; j < randomNum; j++) 
			{
				// if coin face is tails, send the face to callback
				if(isTails)
			    {
					gameEngineCallback.coinFlip(Coin.Face.tails, this);
			    	if(j != randomNum - 1)
			    	{
			    		isTails = false;
			    	}		    	
			    }
				// if coin face is heads, send the face to callback
			    else
			    {
			    	gameEngineCallback.coinFlip(Coin.Face.heads, this);
			    	if(j != randomNum - 1)
			    	{
			    		isTails = true;
			    	}		
			    }
				// set the loop to sleep for 300 milliseconds after
		    	// each coin flip.
				Utilities.sleep(flipDelay);
			}
			// record the outcome of each coin flip to a list	
			if(isTails)
			{
				// if heads, record to the list
				coinFaces.add(Coin.Face.tails);
				// call back the coin flip outcome
				gameEngineCallback.coinFlipOutcome(Coin.Face.tails, this);
			}
			else
		    {
				// if heads, record to the list
				coinFaces.add(Coin.Face.heads);
				// call back the coin flip outcome
				gameEngineCallback.coinFlipOutcome(Coin.Face.heads, this);
		    }				
			
			// delay the next coin flip by half a second 
			Utilities.sleep(coinDelay);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#calculateResult
	 */
	@Override
	public synchronized void calculateResult() 
	{
		Coin.Face firstCoin = coinFaces.get(0);
		Coin.Face secondCoin = coinFaces.get(1);
		int newAmountOfPlayerPoints = 0;
		
		for(Player player : players)
		{
			// check if first and second coins are both heads and player chose heads
			if(firstCoin == Coin.Face.heads && secondCoin == Coin.Face.heads && 
			   player.getFacePick() == Coin.Face.heads)
			{
				player.setResult(GameEngine.GameStatus.WON);
				newAmountOfPlayerPoints = player.getPoints() + player.getBet();
				player.setPoints(newAmountOfPlayerPoints);
			}	
			// check if first and second coins are both tails and player chose tails
			else if(firstCoin == Coin.Face.tails && secondCoin == Coin.Face.tails && 
					player.getFacePick() == Coin.Face.tails)
			{
				player.setResult(GameEngine.GameStatus.WON);
				newAmountOfPlayerPoints = player.getPoints() + player.getBet();
				player.setPoints(newAmountOfPlayerPoints);
			}
			// check if player chose tails with coin draw outcome
			else if(firstCoin == Coin.Face.heads && secondCoin == Coin.Face.tails ||
					firstCoin == Coin.Face.tails && secondCoin == Coin.Face.heads && 
					player.getFacePick() == Coin.Face.tails)
			{
				player.setResult(GameEngine.GameStatus.DREW);
			}
			// check if player chose heads with coin draw outcome
			else if(firstCoin == Coin.Face.heads && secondCoin == Coin.Face.tails ||
					firstCoin == Coin.Face.tails && secondCoin == Coin.Face.heads && 
					player.getFacePick() == Coin.Face.heads)
			{
				player.setResult(GameEngine.GameStatus.DREW);
			}
			// any other case calculates a loss
			else
			{
				player.setResult(GameEngine.GameStatus.LOST);
				newAmountOfPlayerPoints = player.getPoints() - player.getBet();
				player.setPoints(newAmountOfPlayerPoints);
			}
			// callback the result
			gameEngineCallback.gameResult(player, player.getResult(), this);	
			// empty the coinFaces arrayList. This initializes the list for the 
			// next game.
			coinFaces.clear();
		}
	}

	/*
	 * generateRandomNumberInRange() generates a random number
	 * between an arbitrarily chosen minimum and maximum range.
	 * 
	 * @return the random generated integer.
	 */
	public int generateRandomNumberInRange()
	{
		// generate a number based on nano time
		Random rand = new Random(System.nanoTime());
		
		// determine range between 5 and 10
		int minimumRange = 5;
		int maximumRange = 10;		
		int range = maximumRange - minimumRange + 1;
		int randomNumber = rand.nextInt(range) + minimumRange;
		
		return randomNumber;
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#getAllPlayers
	 */
	@Override
	public Collection<Player> getAllPlayers() 
	{
		if(players.isEmpty())
		{
			return null;
		}
		return players;
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#getPlayer
	 */
	@Override
	public synchronized Player getPlayer(String id) 
	{	
		// if list is not null or empty then iterate through
				// and get the player that matches the passed in ID
				// and return it
		if(players != null) 
		{			
			for (Player player : players) 
			{				
				if(id == player.getPlayerId()) 
				{					
					return player;
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#placeBet
	 */
	@Override
	public synchronized void placeBet(Player player, Face face, int bet) throws InsufficientFundsException 
	{
		/*System.out.println("GAME ENGINE IMPL placeBet" +
						   "\nPLAYER NAME: " + player.getPlayerName() + 
				           "\nFACE: " + face + 
				           "\nBET: " + bet);*/
		player.placeBet(face, bet);		
	}

	@Override
	public void removeGameEngineCallback(GameEngineCallback gameEngineCallback) 
	{
		// TODO Auto-generated method stub	
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#removePlayer
	 */
	@Override
	public synchronized boolean removePlayer(Player player) 
	{
		if(players != null) 
		{			
			for (Player p : players) 
			{				
				if(player.getPlayerName() == p.getPlayerName()) 
				{					
					players.remove(p);
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#setPlayerPoints
	 */
	@Override
	public void setPlayerPoints(Player player, int totalPoints)
	{
		player.setPoints(totalPoints);
	}
	
	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#toString(au.edu.rmit.cpt222.model.interfaces.Player)
	 */
	@Override
	public String toString(Player player)
	{		
		return  player.toString();		
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#getBet()
	 */
	@Override
	public int getBet() 
	{
		return player.getBet();
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#resetGameStatus()
	 */
	@Override
	public synchronized void resetGameStatus() 
	{
		player.resetGameStatus();
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#setCurrentPlayer(au.edu.rmit.cpt222.model.interfaces.Player)
	 */
	@Override
	public void setCurrentPlayer(Player player) {
		// TODO Auto-generated method stub
		
	}

	/*
	 * (non-Javadoc)
	 * @see au.edu.rmit.cpt222.model.interfaces.GameEngine#getCurrentPlayer()
	 */
	@Override
	public Player getCurrentPlayer() {
		// TODO Auto-generated method stub
		return null;
	}	
}
