package au.edu.rmit.cpt222.utilities;

import javax.swing.JOptionPane;

/**
 * @author Paolo DiPietro
 * 
 * Utilities class is for static utility methods
 * or methods that are common to numerous classes
 */
public class Utilities 
{
	/*
	 * Method that pops up dialog box to notify the
	 * user about error or game status conditions.
	 */
	public static void showMessage(String theMessage)
	{
		JOptionPane.showMessageDialog(null,theMessage);
		return;
	}
	
	/*
	 * sleep(int time) puts the current executing thread to 
	 * sleep for x amount of time
	 */
	public static void sleep(int time)
	{
		try
		{
			Thread.sleep(time);
		} 
		catch (InterruptedException ex)
		{
			showMessage("Sleep interupted.");
		}
	}
}
