package au.edu.rmit.cpt222.utilities;

import java.io.Serializable;

/**
 * @author Paolo DiPietro
 * 
 * HostDetails encapsulates the host details for each client
 * that needs to connect to the server side for and call back logic
 */
@SuppressWarnings("serial")
public class HostDetails implements Serializable
{
	private String hostName;
	private int port;
	
	public HostDetails(String hostName, int port) 
	{	
		this.hostName = hostName;
		this.port = port;
	}

	public String getHostName() 
	{
		return hostName;
	}

	public int getPort() 
	{
		return port;
	}
}
